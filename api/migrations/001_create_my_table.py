steps = [
    [
        ##create the table
        """
        create table vacations (
            id serial primary key not null,
            name varchar(1000) not null,
            from_date date not null,
            to_date date not null,
            thoughts text
        );
        """,
        ##drop the table
        """
        drop table vacations;
        """
    ]
]